CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

This is a module that creates a form in a modal.
It allows users to subscribe to a newsletter.
The form has 3 fields:

    First Name
    LastName
    Email

It subscribes the user via Ajax to https://sendgrid.com newsletter lists.

This module does not related to SendGrid Integration module.
This module designed to only provide newsletter subscription functionality.

New feature: You can render the form in block.

REQUIREMENTS
------------


This module has a dependency: Ctools


INSTALLATION
------------


Enable the module. Refer to the documentation for help Here.

Define the permissions from /admin/people/permissions 
(Permission name is: Administer SendGrid Newsletter module settings).


CONFIGURATION
-------------

Configure the settings from admin/config/content/sendgrid-newsletter-config path
